class Selector {

	constructor(
		public type: string = "FragmentSelector",
		public conformsTo: string = "http://www.w3.org/TR/media-frags/",
		public value: string
		) { }
}

class Target {
	constructor(
		public id: string,
		public format: string,
		public src: string
		) { }
}

class Body {

	constructor(
		public tags: string,
		public imgTags: string,
		public text: string,
		public purpose: string,
		public station_name: string
		) { }

}


export class Fragment {

  constructor(
    //public id: string,
    public target: Target,
    public body: Body,
    public selector: Selector,
    public creator: string
  ) {  }

}