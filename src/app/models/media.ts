export class Media {

  constructor(
//    public id: number,
    public name: string,
    public contentUrl: string,
    public embedUrl: string,
    public description: string,
    public station_name: string,
    public created: string,
    public type: string,
    public author: string,
    public location: string,
    public skill: string,
    public tags?: string[]
  ) {  }

}