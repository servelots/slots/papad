export class Annotation {

  constructor(
    public id: string,
    public name: string,
    public tags: string,
    public imgTags: string,
    public contentUrl: string,
    public audio_url: string
  ) {  }

}