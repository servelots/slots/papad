import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabview-layout',
  templateUrl: './tabview-layout.component.html',
  styleUrls: ['./tabview-layout.component.scss']
})
export class TabviewLayoutComponent implements OnInit {
  autocompleteItems : any = [];
  constructor() { }

  ngOnInit(): void {
    this.autocompleteItems= ['Item1', 'item2', 'item3']
  }

}
