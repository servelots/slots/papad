import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabviewLayoutComponent } from './tabview-layout.component';

describe('TabviewLayoutComponent', () => {
  let component: TabviewLayoutComponent;
  let fixture: ComponentFixture<TabviewLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabviewLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabviewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
