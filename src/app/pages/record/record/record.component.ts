import { Component, OnInit } from '@angular/core';
import { RecordService } from '../../../services/admin/record.service';
import { StationsService } from '../../../services/stations/stations.service';
import { SelectItem } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AnthillService } from '../../../services/anthill.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {
  recordName: any;
  recordTags: any;
  recordImage: any;
  stationsData: SelectItem[];
  selectedChannel: any = {};
  selectedItem: any = {};
  recordForm : FormGroup;
  submitted: boolean = false;
  audio_url: string;
  img_tags: string;
  tags: any;
  recordData: any=[];
  constructor(private recordService: RecordService,private stationSer: StationsService,private activatedroute: ActivatedRoute,private router: Router,private toaster: ToastrService,private fb: FormBuilder,private anthilSer: AnthillService ) { }

  ngOnInit(): void {
    this.recordForm = this.fb.group({
      audio_url: ['', Validators.required],
      img_tags:['',Validators.required],
      channelSelected: ['',Validators.required],
      tags: ['', Validators.required],
    });
    this.getStationsName();
    this.getRecordData();
  }
  addRecords(){
    this.submitted = true;
    let payload ={
      audio_url: this.recordForm.value.audio_url,
      img_tags: this.recordForm.value.img_tags,
      station_name: this.recordForm.value.channelSelected.station_name,
      tags: this.recordForm.value.tags,
      upload_date: new Date()
    }
    this.recordService.addRecords(payload).subscribe(res => {
      this.toaster.success('Successfully Added New Records');
      this.audio_url ="";
      this.img_tags = "";
      this.selectedChannel = {};
      this.tags = ''
    })
  }
  getStationsName() {
    this.stationSer.getStationData().subscribe(res => {
      this.stationsData = res.map((ele) => ({
        label: ele.station_name,
        value: ele,
        image: ele.station_image,
        id: ele.id
      }));
      this.stationsData.map(ele => {
        if(typeof ele.label !== 'string') return ''
        ele.label = ele.label.charAt(0).toUpperCase() + ele.label.slice(1);
      });
    });
  }
  selectChannel(event) {
   this.selectedChannel = event.value;
   console.log(this.selectedChannel)
  }
  getRecordData(){
    this.anthilSer.getdata().subscribe(res => {
      this.recordData = res;
    })
  }
  receiveMessage(event){
    console.log(event)
    this.audio_url = event.audio_url;
    this.tags =  event.tags;
    this.selectedChannel = event.station;
    this.img_tags=event.img_tags;    
  }
  userClick(){
    this.router.navigate(['pages/add/user']);
  }
  recordClick(){
    this.router.navigate(['pages/add/record']);
  }
  addChannel(){
    this.router.navigate(['pages/add/channel']);
  }
}
