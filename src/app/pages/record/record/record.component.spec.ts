import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RecordComponent } from './record.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { RecordService } from '../../../services/admin/record.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('RecordComponent', () => {
  let component: RecordComponent;
  let fixture: ComponentFixture<RecordComponent>;
  let service: RecordService;
  let de: DebugElement;
  let el : HTMLElement;
   

   const recordData = {
    audio_url: 'aaslll',
    img_tags: 'JanastuimageUrl',
    station_name: 'Janastu',
    tags: 'asdf',
    upload_date: new Date()
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordComponent ],
      imports: [
        RouterTestingModule,HttpClientModule,FormsModule,ReactiveFormsModule
        ,ToastrModule.forRoot()
    ],
    providers: [
      ToastrService,
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordComponent);
    component = fixture.componentInstance;
    service = TestBed.get(RecordService);
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
    fixture.detectChanges();
    fixture.detectChanges();
  });

  it('form should be valid',() => {
    // component.recordForm.controls.selectedChannel =
      
    //     {
    //       "id": 1, 
    //       "station_image": "https://ia601402.us.archive.org/31/items/olipedia-logo/Olipedia_logo.png", 
    //       "station_name": "olipedia"
    //     }
     
     component.selectedChannel = {
      id: 1,
      ​​
      station_image: "https://ia601402.us.archive.org/31/items/olipedia-logo/Olipedia_logo.png",
      ​​
      station_name: "olipedia"
     }
     component.recordForm.controls['audio_url'].setValue('aaslll');
     component.recordForm.controls['img_tags'].setValue('JanastuimageUrl');
     component.recordForm.controls['channelSelected']=component.selectedChannel
     component.recordForm.controls['tags'].setValue('asdf');
     console.log(component.selectedChannel,component.recordForm.controls['channelSelected'].setValue(component.selectedChannel))
 });
 it('should save product details when form is submitted', () => {
    
  component.submitted = true;
  const spy = spyOn(service, 'addRecords').and.returnValue(Observable.empty()
  );
  const form = fixture.debugElement.query(By.css('form'));
  form.triggerEventHandler('submit', null);
 
  expect(spy).toHaveBeenCalled();
  // const button = fixture.debugElement.query(By.css('#save'));
  // button.nativeElement.click();
});
});
