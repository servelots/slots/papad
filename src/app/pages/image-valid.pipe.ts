import { PipeTransform, Pipe } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Pipe({ name: "isImageValid" })
export class ImagePipe implements PipeTransform {
  defaultImage: string = "../assets/images/logo-transparent.png";

  constructor(private http: HttpClient) {}
  
  transform(url: string): any {
    let imageHeaders = ['image/webp', 'image/jpeg', 'image/jpg', 'image/*'];
    let headers = new HttpHeaders({
        'Accept': imageHeaders.join(', ')
    });
    return this.http
      .get(url, {responseType: 'blob' as const, headers: headers})
      .pipe(map(res => {
        //return url if valid
        //BUG: for some reason text/html response is,
        // parsed as blob and have to filter it out like this
        if(res.type == 'text/html') {
          return this.defaultImage;
        }
        return url;
      }),
      catchError(error => {
        //return default image if url not valid
        return of(this.defaultImage);
      }));
  }
}
