import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordingTableComponent } from './recording-table.component';

describe('RecordingTableComponent', () => {
  let component: RecordingTableComponent;
  let fixture: ComponentFixture<RecordingTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordingTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
