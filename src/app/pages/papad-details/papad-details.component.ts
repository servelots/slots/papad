import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnthillService } from '../../services/anthill.service';
import { ToastrService } from 'ngx-toastr';
import { Lightbox } from 'ngx-lightbox';
import { AuthStore } from './../../authentication/auth-store';
import { StationsService } from '../../services/stations/stations.service'
@Component({
  selector: 'app-papad-details',
  templateUrl: './papad-details.component.html',
  styleUrls: ['./papad-details.component.scss']
})
export class PapadDetailsComponent implements OnInit {
  audioPlay: boolean = false
  data: any = [];
  currentEvent: any = {};
  event: number;
  imageUrl: any = [];
  suggestedTag: any = [];
  msbapTitle: any;
  msbapAudioUrl: any;
  msbapDisplayTitle: boolean = false;
  msbapDisplayVolumeControls: boolean;
  changeTag: String;
  toggle: boolean = true;
  selectedArray: any = [];
  selectedImage: any = {}
  progressbar: boolean = false;
  correctIcon: boolean = false;
  uploadImage: boolean = false;
  url: string | ArrayBuffer;
  playAudio: boolean = false;
  sample: any = [];
  album: any = [];
  constructor(private activatedroute: ActivatedRoute, private antHillservice: AnthillService, private toaster: ToastrService, private router: Router, private _lightbox: Lightbox, private authStore: AuthStore, private stationSer: StationsService) {
  }

  ngOnInit(): void {
    let x = this.activatedroute.snapshot.params.event;
    this.event = parseInt(x);
    this.getPapaddetails();
    let url = this.router.url.split('/')[1]
    if (this.router.url === '/pages/audio-details/' + this.event) {
      this.playAudio = true;
      // document.getElementById('audio')[0].play()
    }
    else if (this.router.url === '/pages/papad-details/' + this.event) {
      this.playAudio = false;
    }
  }
  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.album, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }
  getPapaddetails() {

    this.antHillservice.getdata().subscribe(res => {
      this.data = res;
      let temp = {}
      this.data.forEach(item => {
          if (this.event === item.id) {
            console.log(item);
           this.suggestedTag= item.tags.split(',')
          this.currentEvent = item;
          this.msbapTitle = 'Audio Title';
          this.msbapAudioUrl = this.currentEvent.audio_url;
          this.msbapDisplayTitle = false;
          this.msbapDisplayVolumeControls = true;
          this.imageUrl = item['img_tags'].split(',');
          this.imageUrl.forEach(key => {
            this.compImgUrl.forEach(ele => {
              ele.url = key;
              ele.icon = false;
              this.dummyArray.push(ele)
            })
          })
        }
      })

    })
  }
  onSave() {
    let sendUrl = this.imageUrl.toString();
    let x;
    if (this.changeTag === undefined) {
      x = this.suggestedTag.toString()
    }
    else {
      x = this.sample.toString()
    }
    if (this.sample.length === 0) {
      x = this.suggestedTag.toString()
    }
    let payload = {
      station_name: this.currentEvent.station.station_name,
      tags: x,
      upload_date: new Date(),
      audio_url: this.msbapAudioUrl,
      img_tags: sendUrl,
    }
    this.antHillservice.updateItem(this.currentEvent.id, payload).subscribe(res => {
      this.showSuccess('Successfully Updated');
      this.changeTag = '';
      this.album = [];
      this.authStore.getFullData();
      setTimeout(() => {
        this.getPapaddetails();
      }, 500)
    })
  }
  showSuccess(msg: string) {
    this.toaster.success(msg);
  }
  showerror(msg) {
    this.toaster.error(msg);
  }
  sugestedClick(event) {
    this.sample.push(event.value)
    this.suggestedTag = this.sample.filter((a, b) => this.sample.indexOf(a) === b);
    // let temp: any = [];
    // temp = event.split(',');
    // this.sample = temp;
    // this.changeTag = event.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // this.sample = this.suggestedTag.filter(val => !temp.includes(val));
    // this.sample = this.suggestedTag.concat(temp);
    // this.sample = this.sample.filter((a, b) => this.sample.indexOf(a) === b);
  }

  onSelectFile(event) {
    //  event - DOM Element
    this.progressbar = true
    setTimeout(() => {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url  
        reader.onload = (event) => {
          this.url = event.target.result;
          this.progressbar = false
          this.uploadImage = true;
        }
      }
    }, 6000);
  }
  onSuggestedTag(item, element) {
    // "item" is the selected element or object and 'element' is a DOM element
    element.classList.toggle("badge-success");
    this.selectedArray.push(item);
    this.selectedArray = this.selectedArray.filter((ele, _, array) => array.indexOf(ele) === array.lastIndexOf(ele))
    this.changeTag = this.selectedArray.toString();
  }
  onClickImage(selectedImage, i) {
    this.imageUrl.forEach((item, index) => {
      if (item === selectedImage && index === i) {
        this.correctIcon = true
      }
    })
  }
  onCancel() {
    // ******************  ROUTING TO HOME PAGE ****************
    this.stationSer.stationData = {}
    this.router.navigate(['pages/papad'])
  }
  imageUrlAss(event) {
    this.imageUrl.push(event);
  }
}
