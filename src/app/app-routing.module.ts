import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { PagesModule } from './pages/pages.module'
//import { RefreshComponent } from './refresh/refresh.component';
import { ChannelsLayoutComponent } from './channels-layout/channels-layout.component';
import {TabviewLayoutComponent} from './tabview-layout/tabview-layout.component'
import {SyncComponentComponent} from './sync-component/sync-component.component'
import { FileUploadComponent } from './file-upload/file-upload.component';
import {ItemDetailComponent} from './item-detail/item-detail.component';
import { HomeComponent } from './home/home.component';
import { ConnectionsComponent } from './connections/connections.component';

/*const routes: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: 'pages', loadChildren: './pages/pages.module#PagesModule' },
  {path:'refresh',component:RefreshComponent}
];*/

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'connections', component: ConnectionsComponent },
  { path: 'item/:itemId', component: ItemDetailComponent },
  { path: 'ingest', component: FileUploadComponent },
  { path: 'channels/:channelId', component: ChannelsLayoutComponent },
  { path: '', redirectTo: 'home', pathMatch: "full" },
  { path:'view', component:TabviewLayoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
