import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tag-item',
  template: `
    <span (click)="toggleSelected()" [ngClass]="selected ? 'badge-secondary' : 'badge-primary'" class="tag-item badge">

    {{ value }}
    </span>
  `,
  styleUrls: ['./tag-item.component.scss']
})
export class TagItemComponent implements OnInit {

  constructor() { }

  @Input() value: string;
  @Input() selected: boolean;
  @Output() selectToggled: EventEmitter<any> = new EventEmitter();
  
  toggleSelected() {
    this.selected = !this.selected;
    this.selectToggled.emit(this);
  }

  ngOnInit(): void {
  }
}
