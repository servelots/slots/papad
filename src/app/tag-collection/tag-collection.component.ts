import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnthillService } from '../services/anthill.service';
import { environment } from '../../environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-tag-collection',
  templateUrl: './tag-collection.component.html',
  styleUrls: ['./tag-collection.component.scss']
})
export class TagCollectionComponent implements OnInit {
  channel: string = environment.deviceId;

  constructor(private _apiservice: AnthillService,
              private _route: ActivatedRoute,
              private _router: Router) {

  this._route.paramMap.subscribe(params => {
      this.updateSelected(params['params']);
    });
  }

  // input received from other components ex. router
  selectedTags: any[] = [];// = ["Addition", "Basic", "covid"];

  // sorted array of tags with selected state
  tags: any[] = [];
  
  apiData: any[] = [];
  channelByRoute: string = "";
  updateSelected(params) {
    if(params['tags']){
      this.selectedTags = params.tags.split(',');
    }
    if(params['channelId']){
      this.channelByRoute = params['channelId'];
    }
  }
  
  getData() {
    // fetch data from service
    this._apiservice.getByChannel(this.channelByRoute || this.channel).subscribe(res => {
      this.apiData = [...this.apiData, ...res],
      this.parse();
    });
    this._apiservice.getAllFragsByChannel(this.channelByRoute || this.channel).subscribe(res => {
      this.apiData = [...this.apiData, ...res],
      this.parse();
    });
  }

  parse() {
    // console.log(this.apiData, "api data");
    // parse data to extract relevance data for this component
    let tagsArrayOfArrays = this.apiData.map(function(item){
      let TagsToCheck;
      if(item.body)  {
        TagsToCheck = item.body.tags;
      } else {
        TagsToCheck = item.tags;
      }
      
      
          if(TagsToCheck){
            if(TagsToCheck.indexOf(',') > -1) {
              return TagsToCheck.trim().split(',');
            }else {
              // console.log(item, TagsToCheck, TagsToCheck.trim().split(' '))
              return TagsToCheck.trim().split(' ');
            }
          }
          
      });
    // console.log(tagsArrayOfArrays, "tags array")
    let arrayOfUniqTags = _.uniq(_.compact(_.flatten(tagsArrayOfArrays)));
    let groupedByTag = _.groupBy(_.compact(_.flatten(tagsArrayOfArrays)), function(tag) {
      return tag.trim();
    });
    // remove the default tag with value tags
    delete groupedByTag.tags;

    let reducedWithCount = _.reduce(groupedByTag, function(result, value, key){ 
                              (result[key] || (result[key] = [])).push(key, value.length);
                              return result;
                        }, {});
    let sortByCount = _.sortBy(reducedWithCount, function(o){
                          return -Number(o[1])
                      });
    console.log(sortByCount, "tags by count");
    this.interpret(sortByCount);
  }

  interpret(tags: any[]) {
    // data transformation as per the ui / ux requirement
    // ex: To check if there is any selected tags via 
    // url params and update local state 
    //console.log("xform")
    let transformedTags = tags.map(function(tag){
    
      if(this.selectedTags.indexOf(tag[0].trim()) >= 0) {
        //tag["value"] = tag[0].trim();
        //tag["isSelected"] = true;

        return {value: tag[0].trim().slice(0, 35) +" (" + tag[1] +")", isSelected: true};
      } else {
        return {value: tag[0].trim().slice(0, 35) + " (" + tag[1] +")", isSelected: false};
        //tag["value"] = tag[0].trim();
        //tag["isSelected"] = true;
      }
      //return tag;     
    }, this); 

    transformedTags = _.sortBy(_.union(this.tags, transformedTags), function(o){
                          let count = o.value.slice(-2, -1);
                        
                          return -Number(count);
                      });
    this.tags = _.uniqBy([...transformedTags], function(tag){
      return tag.value.split(' ')[0];
    });
  }

  onTagClick(event) {
   
    var cleanedValue = event.value.split(" (")[0];

    if(event.selected){

      this.selectedTags.push(cleanedValue);  
    } else {
      let index = this.selectedTags.indexOf(cleanedValue);
      this.selectedTags.splice(index, 1);
    }
    
    this.selectedTags = _.clone(this.selectedTags);
  
    this._router.navigate(['/channels/'+this.channelByRoute, 
          { tags: this.selectedTags.join(',') }]);
    
    if(this.selectedTags.length === 0) this._router.navigate(['/channels/'+this.channelByRoute]);
  }

  ngOnInit(): void {
    this.getData();
  }
}
