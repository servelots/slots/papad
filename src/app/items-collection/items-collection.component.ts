import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input, ViewChild, 
         ElementRef } from '@angular/core';
import { environment } from '../../environments/environment';
import { AnthillService } from '../services/anthill.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FileUploadValidators } from '@iplab/ngx-file-upload';
import { IsLoadingService } from "@service-work/is-loading";
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

@Component({
  selector: 'app-items-collection',
  templateUrl: './items-collection.component.html',
  styleUrls: ['./items-collection.component.scss']
})
export class ItemsCollectionComponent implements OnInit {
  private filesControl = new FormControl(null, FileUploadValidators.filesLimit(1));
  public fileIndex = environment.files;
  public addCategoryForm = new FormGroup({
      icon: this.filesControl,
      value: new FormControl('', Validators.required),
  });
  @ViewChild('closeUpload') closeUpload: ElementRef;
  private uploadUrl: string = environment.uploadUrl;
  constructor(
          private _http: HttpClient,
          private _apiservice: AnthillService,
  			  private _route: ActivatedRoute,
          private toaster: ToastrService, private isLoadingService: IsLoadingService,
          private router: Router) { 
        
  	this._route.paramMap.subscribe(params => {
  			console.log(params, "route params from items");
  			this.updateFromQuery(params['params']);
  	});
  }
  // input received from other components ex. router
  tagsForFilter: any[] = []; // = ["Addition", "Basic", "covid"];
  channelsForFilter: string = "all";
  annotations: any[];
  stagedData: any[];
  allFragments: any[];
  categories: any[];
  // possible re-factor
  updateFromQuery(queryParams) {
    if(queryParams['skill'] || queryParams['location']){
      this.getFilteredData({'skill': queryParams['skill'], 'location': queryParams['location']});
      //console.log(queryParams['skill'], queryParams['location']);
    }
  	else if(queryParams['tags']){
  		this.tagsForFilter = queryParams['tags'].split(',');
      //console.log("tag param")
  		//this.filterByTags();
      this.getData();
      
  	} else if(queryParams['channelId']) {
  		this.channelsForFilter = queryParams['channelId'];
      console.log(this.channelsForFilter, "channels for filter");
      //console.log("channel param")
  		//this.filterByChannels();
      this.getData();
  	}
  	else {
      this.getData();
      //console.log("else param")
  		//this.stagedData = this.annotations;
  	}
  }

  getData() {
    // fetch data from service
    this._apiservice.getdata().subscribe(res => {
      this.annotations = _.reverse(res);
      //console.log(res, _.reverse(res), "from call");
      if(this.tagsForFilter.length > 0) {
      	this.filterByTags();
      } else if(this.channelsForFilter.length > 0) {
      	this.filterByChannels();
      }
      else {
      	this.stagedData = this.annotations;
      }
      this.getCategories();
      this._apiservice.getAllFragments().subscribe(res => {
        console.log(res);
        this.allFragments = _.reverse(res);
        if(this.tagsForFilter.length > 0) {
          this.filterByTags();
        }
        
      });


    });

   
    return;
  }
  getFilteredData(filterParams:any) {
    this._apiservice.getFiltered(filterParams).subscribe(res => {
      this.stagedData = res;
    });
  }
  sanitizeTags(tags: string) {
  	
  	let tagsArray;
    if(tags){
      if(tags.indexOf(',') > -1) tagsArray = tags.split(',');
      else tagsArray = tags.split(" "); 
    }

  	let tagsInLowercase = _.map(tagsArray, function(tag){
  		return tag.toLowerCase().trim();
  	});
    console.log(tags, tagsInLowercase, "sanitizer");
  	return tagsInLowercase;
  }
  filterByTags() {
  	//cards filter logic goes here
  	let that = this
  	let filteredAnnos = _.map(that.annotations, (anno => {
  		// iterating annos data
      let annoTags;
      annoTags = this.sanitizeTags(anno.tags);
  		
      
  		let isTrue = false;
  		// iterate over the selected tags array
  		_.each(this.tagsForFilter, (tag => {
  			console.log(annoTags.indexOf(tag.toLowerCase().trim()), tag);
  			let doesExist = annoTags.indexOf(tag.toLowerCase().trim());
  			if(doesExist >= 0) {
  				isTrue = true;
  			}
  		}));

  		if(isTrue) {
  			console.log("filtered");
  			return anno;
  		} else {
  			return null;
  		}

  	}));
    console.log(that.allFragments);
    let filteredFragments = _.map(that.allFragments, (frag => {
          let fragTags;
          fragTags = this.sanitizeTags(frag.body.tags);
          console.log(this.tagsForFilter, "filtered frag")
          
          let isTrue = false;
          // iterate over the selected tags array
          _.each(this.tagsForFilter, (tag => {
            console.log(fragTags.indexOf(tag.toLowerCase().trim()), tag);
            let doesExist = fragTags.indexOf(tag.toLowerCase().trim());
            if(doesExist >= 0) {
              isTrue = true;
            }
          }));

          if(isTrue) {
            console.log("filtered");
            let final = that.annotations.find( anno => {
              if(anno.contentUrl === frag.target.src) {
                return anno;
              }
            });
            console.log("filtered frag", final);
            return final;
          } else {
            return null;
          }
    }));
    console.log(_.compact(filteredFragments), "filtered frag")
  	// setup filtered data for rendering
  	this.stagedData = _.union(_.compact(filteredAnnos), _.compact(filteredFragments));
  }

  filterByChannels() {
  	let that = this;
  	let filteredAnnos = _.map(that.annotations, (anno => {
  		if(this.channelsForFilter !== "all"){
  			return anno.station_name.toString() == that.channelsForFilter ? anno : null;
  		} else {
  			return anno;
  		}
  		
  	}));
  	this.stagedData = _.compact(filteredAnnos);
    console.log("in filter by channels");
  }

  gotToDetailPage(anno) {
    console.log(anno.id, anno);
    var annoId = anno._id["$oid"];
    this.router.navigate(['/item/'+annoId]);
  }

  getCategories() {
    this._apiservice.getCategories().subscribe(res => {
      this.categories = res;
      console.log(res, "categoreis");
    });
  }

  addCategory() {
    console.log(this.addCategoryForm.getRawValue(), "check form value");
    let formVals = this.addCategoryForm.getRawValue();
    this.isLoadingService.add({ key: ["default", "single"] });
    if(formVals.icon[0]){
      const formData = new FormData();
      formData.append('file', formVals.icon[0]);
      this._http.post<any>(this.uploadUrl, formData).subscribe(
        (res) => {
          let iconFile = res.url.split('/').slice(-1)[0];
          this._apiservice.addCategory({type:"category", icon: iconFile, value: formVals.value }).subscribe(res => {
                this.categories = [...this.categories, res[0]];
                this.isLoadingService.remove({ key: ["default", "single"] });
                this.closeUpload.nativeElement.click();
                this.toaster.success('Successfully added Category');
                this.addCategoryForm.reset();
                console.log(res, "categoreis");
          });
          console.log(res, iconFile, "file uploaded")
          
        },
        (err) => console.log(err, "error")
        //this.toaster.success('Successfully uploaded File');
      );
    }
    /*this._apiservice.addCategories().subscribe(res => {
      this.categories = res;
      console.log(res, "categoreis");
    });*/
  }

  ngOnInit(): void {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;

  }

}
