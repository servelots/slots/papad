import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Media } from '../models/media';
import { RecordService } from '../services/admin/record.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-ingest-form',
  templateUrl: './ingest-form.component.html',
  styleUrls: ['./ingest-form.component.scss']
})
export class IngestFormComponent implements OnInit {
  constructor(private recordService: RecordService,
              private toaster: ToastrService) { }
  @Input() contentUrl: string;
  @Input() type: string;

  submitted: boolean = false
  private deviceId: string = environment.deviceId;
  model = new Media('file name', 
                    this.contentUrl, 
                    '', 'description', 
                    this.deviceId, 
                    new Date().toString(), 
                    this.type,
                    '', '', '', 
                    ['keywords', 'tags'])

  //@Output() onSubmitted = new EventEmitter<boolean>();;

  onSubmit() {
  	
  	console.log(this.submitted, this.model, "submitted");
    this.recordService.addRecords(this.model).subscribe(res => {
      this.toaster.success('Successfully Added New Records');
      //this.reset();
      this.submitted = !this.submitted;
      //this.onSubmitted.emit(true);
      this.reset();
    })

  }

  reset() {
    this.model = new Media(' ', 
                           this.contentUrl, 
                           '', '', this.deviceId, 
                           new Date().toString(), 
                           '', '', '', '', [''])
    this.submitted = false;
  }
  ngOnInit(): void {
    console.log(this.contentUrl, "content url")
    this.model.contentUrl = this.contentUrl
    this.model.type = this.type
  }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }
}
