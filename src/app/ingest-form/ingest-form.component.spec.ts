import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngestFormComponent } from './ingest-form.component';

describe('IngestFormComponent', () => {
  let component: IngestFormComponent;
  let fixture: ComponentFixture<IngestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
