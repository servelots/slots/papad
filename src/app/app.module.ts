import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {TabViewModule} from 'primeng/tabview';
import { RefreshComponent } from './refresh/refresh.component';
import { LightboxModule } from 'ngx-lightbox';
import { AuthStore } from './authentication/auth-store';
import { TagItemComponent } from './tag-item/tag-item.component';
import { TagCollectionComponent } from './tag-collection/tag-collection.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { ItemsCollectionComponent } from './items-collection/items-collection.component';
import { ChannelsLayoutComponent } from './channels-layout/channels-layout.component';
import { ImagePipe } from './pages/image-valid.pipe';
import { ChannelDropdownComponent } from './channel-dropdown/channel-dropdown.component';
import { TabviewLayoutComponent } from './tabview-layout/tabview-layout.component';
import { ListItemComponent } from './list-item/list-item.component';
import { ListCollectionComponent } from './list-collection/list-collection.component';
import { SyncComponentComponent } from './sync-component/sync-component.component';
import { AnnoFormComponent } from './anno-form/anno-form.component';
import { IngestFormComponent } from './ingest-form/ingest-form.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { HomeComponent } from './home/home.component';
import { FilterComponent } from './filter/filter.component';
import { ConnectionsComponent } from './connections/connections.component';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { FileUploadComponent } from './file-upload/file-upload.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { CategoriesComponent } from './categories/categories.component'; 

// Application wide providers
const APP_PROVIDERS = [
  AuthStore
];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RefreshComponent,
    TagItemComponent,
    TagCollectionComponent,
    ItemCardComponent,
    ItemsCollectionComponent,
    ChannelsLayoutComponent,
    ImagePipe,
    ChannelDropdownComponent,
    TabviewLayoutComponent,
    ListItemComponent,
    ListCollectionComponent,
    SyncComponentComponent,
    AnnoFormComponent,
    IngestFormComponent,
    ItemDetailComponent,
    HomeComponent,
    FilterComponent,
    ConnectionsComponent,
    FileUploadComponent,
    CategoriesComponent,
  ],
  imports: [
    NgbModule,
    FormsModule ,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    DropdownModule,
    TabViewModule,
    LightboxModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    FileUploadModule,
    MatProgressBarModule,
    ToastrModule.forRoot({
      timeOut: 7000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
    }),
    ToastContainerModule,
    HttpClientModule,
    NgxAudioPlayerModule,
    TagInputModule,
    NgxSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [TagItemComponent, TagCollectionComponent, ItemCardComponent, ItemsCollectionComponent, ChannelsLayoutComponent, ]
})
export class AppModule { }
