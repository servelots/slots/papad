import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnthillService {
  private recordings: string = environment.apiUrl.getAnnos.base_getAnnos + 'recordings/';
  private fragments: string = environment.apiUrl.getAnnos.base_getAnnos + 'fragments/';
  private categories: string = environment.apiUrl.getAnnos.base_getAnnos + 'categories/';
  private channel: string = environment.deviceId;

  constructor(private _http: HttpClient) { }
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  getdata(): Observable<any> {
    return this._http.get(this.recordings)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getByChannel(channel): Observable<any> {
    return this._http.get(this.recordings+"?station_name="+ channel)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getFiltered(filterParams): Observable<any> {
    console.log(filterParams, "from service")
    let url;
    if(filterParams['skill']) {
       url = this.recordings + '?skill=' + filterParams['skill'];
    }
    if(filterParams['location']){
      url = this.recordings + '?location=' + filterParams['location'];
    }
    if(filterParams['skill'] && filterParams['location']){
      url = this.recordings + '?skill=' + filterParams['skill'] + '&location=' + filterParams['location'];
    }
    return this._http.get(url)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getItem(id): Observable<any> {
    return this._http.get(this.recordings+id)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  // ADD DATA
  addData(data): Observable<any> {
    return this._http.post(this.recordings + 'record/post', data).pipe(map((response: Response) => response),
      catchError(this.handleError)
    )
  }
  // Update item by id
  updateItem(id, data): Observable<any> {
    return this._http.put(this.recordings + id, data)
      .pipe(
        map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getAllFragments(): Observable<any> {
    return this._http.get(this.fragments)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getAllFragsByChannel(channel): Observable<any> {
    var query = {"$regex": ".*"+ channel +".*"};
    return this._http.get(this.fragments+"?body.station_name="+JSON.stringify(query))
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )

  }
  getFragments(src): Observable<any> {
    return this._http.get(this.fragments+"?target.src="+src)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getCategories(): Observable<any> {
    return this._http.get(this.categories)
    .pipe(map((response: Response) => response),
    catchError(this.handleError)
    )
  }
  addCategory(data): Observable<any> {
    console.log(data, "from add cat service");
    return this._http.post(this.categories, data)
          .pipe(map((response: Response) => response),
          catchError(this.handleError)
          )
  }

  private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'Record Service error');
  }
}
