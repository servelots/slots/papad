import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { AnthillService } from './anthill.service';

describe('AnthillService', () => {
  let service: AnthillService;

  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [HttpClientTestingModule], 
    });
    service = TestBed.inject(AnthillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
