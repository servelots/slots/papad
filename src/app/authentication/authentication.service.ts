import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private baseUrl: string = environment.apiUrl.getAnnos.base_getAnnos;
  constructor(private _http: HttpClient) { }
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  getdata(): Observable<any> {
    return this._http.get(this.baseUrl)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'Authentication Service error');
  }
}
