import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FileUploadValidators } from '@iplab/ngx-file-upload';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Media } from '../models/media';    
import { RecordService } from '../services/admin/record.service';
import { ToastrService } from 'ngx-toastr';
import { IsLoadingService } from "@service-work/is-loading";

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  
  public animation: boolean = false;
  public multiple: boolean = false;
  private uploadUrl: string = environment.uploadUrl;
  public targetUrl: string;
  public type: string;
  public submitted: boolean = false;
  private deviceId: string = environment.deviceId;

  private filesControl = new FormControl(null, FileUploadValidators.filesLimit(1));

  public uploadForm = new FormGroup({
      file: this.filesControl,
      name: new FormControl('File Name', Validators.required),
      description: new FormControl('Add Description'),
      author: new FormControl('Author', Validators.required),
      location: new FormControl('Place name'),
      skill: new FormControl('Skills'),
      tags: new FormControl('tags', Validators.required)
  });
  constructor(private _http: HttpClient,
              private recordService: RecordService,
              private toaster: ToastrService, private isLoadingService: IsLoadingService,
              private router: Router) {  }

  public clear(): void {
      //this.uploadedFiles = [];
      this.filesControl.setValue([]);
  }
  public onSubmit() {
  	console.log(this.uploadForm.getRawValue(), "check form value");
  	if(this.uploadForm.getRawValue().file[0]) {
  		const formData = new FormData();
  		
  		formData.append('file', this.uploadForm.getRawValue().file[0]);
  		formData.append('name', this.uploadForm.getRawValue().file[0].name);
  		
      this.isLoadingService.add({ key: ["default", "single"] });
  		this._http.post<any>(this.uploadUrl, formData).subscribe(
  			(res) => {
  				this.targetUrl = res.url;
          this.sendAnno();
  				console.log(res, this.targetUrl, "file uploaded")
  				
  			},
  			(err) => console.log(err, "error")
  			//this.toaster.success('Successfully uploaded File');
  		);
  		
  	}
  	
  }
  sendAnno() {
    console.log(this.uploadForm, this.uploadForm.getRawValue(), "from send annp");
    let filename = this.targetUrl.split('/').slice(-1)[0];
    let ext = filename.split('.')[1] || '';
    let payload = new Media(this.uploadForm.getRawValue()['name'], 
                    filename, 
                    '', this.uploadForm.getRawValue()['description'], 
                    this.deviceId, 
                    new Date().toString(), 
                    //Bug fix: try other ays to get the file extension
                    // tried a different method with split
                    //filename.slice(-3),
                    ext,
                    this.uploadForm.getRawValue()['author'], 
                    this.uploadForm.getRawValue()['location'], 
                    this.uploadForm.getRawValue()['skill'], 
                    this.uploadForm.getRawValue()['tags']);
    this.recordService.addRecords(payload).subscribe(res => {
      console.log(res, "response");
      this.toaster.success('Successfully Added New Records');
      this.isLoadingService.remove({ key: ["default", "single"] })
      //this.reset();
      this.submitted = !this.submitted;
      //this.onSubmitted.emit(true);
      this.clear();
      this.uploadForm.reset();
      this.navigateToItem(res);
    });
  }
  navigateToItem(res) {
    console.log(res);
    let that = this;
    let itemId = res[0]['_id']['$oid'];
    let delay = setInterval(function(){
      that.router.navigate(['/item/'+itemId]);
    }, 3000);
    
  }
  ngOnInit(): void {
  }

}
