import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StationsService } from '../services/stations/stations.service';
import { SelectItem } from 'primeng/api';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-channel-dropdown',
  templateUrl: './channel-dropdown.component.html',
  styleUrls: ['./channel-dropdown.component.scss']
})
export class ChannelDropdownComponent implements OnInit {
  
  constructor(public apiService: StationsService,
              private router: Router,
              private _route: ActivatedRoute,) {

    this._route.paramMap.subscribe(params => {
       
      this.setActive(params['params']['channelId']);
    
    });

   }
  stationsData: SelectItem[];
  selectedItem: SelectItem;
  selectedChannel: any = {};
  item: any = [];
  recordData: any =[];
  channelData: any = [];
  currentStation: string;

  ngOnInit(): void {
    this.getStationsName();
  }
  getStationsName() {
    this.apiService.getStationData().subscribe(res => {
      this.stationsData = res.map((ele) => ({
        label: ele.station_name,
        value: ele,
        image: ele.station_image,
        id: ele.id
      }));
      this.stationsData.map(ele => {
        if(typeof ele.label !== 'string') return ''
        ele.label = ele.label.charAt(0).toUpperCase() + ele.label.slice(1);
      });
      this.setActive(this.currentStation || environment.deviceId);
    });
  }
  setActive(channel) {
    this.currentStation = channel;
    if(channel) {
      this.selectedItem = this.stationsData.find(function(item){
        return item.label == channel;
      });
    }
    
    console.log(channel, "current station");
  }
  selectChannel(event) {
    console.log(event, "dropdown");
    this.setActive(event.station_name);
    
    this.router.navigate(['/channels', event.station_name]);
    /*this.apiService.stationData = event;
    
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate(['channels', event.id]));
      
      this.recordData = JSON.parse(sessionStorage.getItem('RecordData'));
      this.channelData = this.recordData.filter((item) => event.id === item.station.id);
      sessionStorage.setItem('ChannelData',JSON.stringify(this.channelData))
      */
  }
}
