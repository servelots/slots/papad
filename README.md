# PapadApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Introduction

An application designed to render the recorded audio recording along with tags and ability to upload audio recording , tag the audio with relevent texts and images. And also annotate the audio with text or image in the audio timeline.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Papad Documentation
The link for Papad Documentation [here](https://pad.riseup.net/p/Papad_Doc-keep).

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Todo After cloning

After cloning,

Run `npm i` to install the available packages in package.json

Run  `ng serve -o` to run the server

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Setup for Angular

    1.Node Js Installation

    2.Angular-cli

##  Technology Used

    Angular-9

    Bootstrap-4

    Primeng

## File Structure

    .

    ├── angular.json

    ├── anthillhack.json

    ├── browserslist

    ├── e2e

    ├── karma.conf.js

    ├── node_modules

    ├── package.json

    ├── package-lock.json

    ├── package-lock.json.1656308871

    ├── README.md

    ├── src

    │   ├── app

    |         ├── app.component.html   

    |         ├── app.component.scss

    |         ├── app.component.spec.ts

    |         ├── app.component.ts

    |         ├── app.module.ts

    |         ├── app-routing.module.ts

    |         ├── navbar

    |         ├── pages

    |         ├── services

    │   ├── assets

    │   ├── environments

    │   ├── favicon.ico

    │   ├── index.html

    │   ├── main.ts

    │   ├── polyfills.ts

    │   ├── styles.scss

    │   └── test.ts

    ├── tsconfig.app.json

    ├── tsconfig.json

    ├── tsconfig.spec.json

    └── tslint.json













